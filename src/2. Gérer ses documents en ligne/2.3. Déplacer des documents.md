# Déplacer des documents

### D'un coup d'œil

<video width="640" height="480" controls>
  <source src="copier-plusieurs-fichiers-menu.mp4" type="video/mp4">
</video>

### Étape par étape

1.  Cochez les fichiers à copier ou déplacer dans la liste des dossiers.

2.  Ouvrez le menu "Actions" dont le bouton apparaît en haut de la liste dès qu'un fichier
    ou dossier est sélectionné.

3.  Choisissez "Déplacer ou copier" dans le menu déroulant.

    ![](deplacer-plusieurs-fichiers-menu.png)

    -   Vous pouvez ensuite parcourir l'arborescence de votre espace
        Nextcloud (l'icône « maison » représente la racine de votre
        dossier personnel).

4.  Choisissez le dossier de destination.

5.  Choisissez de copier (le document reste présent dans le dossier
    d'origine) ou de déplacer (le document est supprimé du dossier
    d'origine).

6.  Patientez quelques secondes.

Déplacer par cliquer-glisser
----------------------------
<video width="640" height="480" controls>
  <source src="deplacer-cliquer-glisser.mp4" type="video/mp4">
</video>

1.  Cochez les fichiers à copier ou déplacer dans la liste des dossiers.
2.  Cliquez sur l'un des fichiers puis déplacez la souris sans lâcher le
    bouton gauche. Déposez les fichiers sur le dossier de votre choix.
---

➡️ Continuer en [supprimant et restaurant des fichiers](<2.4. Supprimer et restaurer des fichiers.md>).
