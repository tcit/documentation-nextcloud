# Collectifs

Le module Collectifs permet de créer des groupes de travail autour de **documents partagés**, éditables en commun. 

Il est aussi possible d'y associer un [agenda](<../7. Agenda/7.3. Partager un agenda.md>) ou un [carnet d'adresses](<../6. Contacts/6.3. Partager un carnet d'adresses.md>).

------------------------------------------------------------------------

➡️ Continuer en [créant un
collectif](<../5. Collectifs/5.1. Créer un collectif.md>).
