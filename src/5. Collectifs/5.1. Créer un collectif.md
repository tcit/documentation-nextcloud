# Créer un collectif

1. Cliquez sur le module **Collectifs** dans la barre des modules en haut à gauche.

   ![](creer-collectif.png)
2. Cliquez sur "Créer un nouveau collectif" dans le menu à gauche.
3. Donnez un nom à votre collectif.
   * Vous pouvez ajouter un émoji pour le reconnaître rapidement en cliquant sur l'icône à gauche du champ de saisie.

     ![](valider.png)
4. Cliquez sur la flèche à droite du champ de saisie pour valider.

------------------------------------------------------------------------

➡️ Continuer en [ajoutant des membres](<../5. Collectifs/5.2. Ajouter des membres.md>) au collectif.
