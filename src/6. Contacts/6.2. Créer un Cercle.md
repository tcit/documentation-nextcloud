Créer un Cercle
===============

Un cercle sert à **grouper des utilisateur⋅ices** de votre instance Nextcloud, dans l'idée de **partager** un agenda, des contacts, une
discussion, des notes (grâce au module Collectif), une liste de tâches
(grâce au module Deck) et des fichiers. Autrement dit, à **organiser du
travail collectif**.

1.  Cliquez sur le bouton **+** de la section Cercles dans la liste des
    contacts.

    ![](nouveau-cercle.png)

2.  Une boîte de dialogue apparaît. Donnez un nom au cercle et cliquez sur "Créer
    un cercle".

    ![](nommer-cercle.png)

3.  Cliquez sur "Ajouter des membres" en haut du menu, à gauche de la vue principale.

    ![](ajouter-membres.png)

4.  Une boîte de dialogue apparaît. Commencez à taper le nom de membres à ajouter dans le champ de saisie et cliquez dessus pour
    les sélectionner.

    ![](chercher-membres.png)

Gérer les rôles
---------------

Quand vous créez un Cercle, vous en êtes automatiquement **propriétaire
(*Owner*)**.

Vous pouvez accorder différents droits aux autres membres de la
discussion en cliquant sur le menu d'actions à droite de leur nom, dans la liste des membres.

![](roles-membres.png)

-   Modérateur⋅ice (*Moderator)* : donne l'autorisation d'ajouter et de
    supprimer des **membres** du cercle.

-   Administrateur⋅ice (*Admin*) : donne l'autorisation de **modifier
    les paramètres** et d'ajouter et supprimer des **membres** et des
    **modérateu⋅ices** du cercle.

-   *Promouvoir en tant que seul propriétaire* : **remplace** **le⋅a
    propriétaire actuel⋅le**, donne l'autorisation de **supprimer le
    cercle**, de **modifier les paramètres** et d'ajouter et supprimer
    des **membres**, des **modérateur⋅ices** et des
    **administrateur⋅ices** du cercle.

    > ℹ️ Dans le cas où le compte propriétaire est supprimé de
    > Nextcloud, l'administrateur⋅ice le⋅a plus ancien⋅ne du cercle
    > devient propriétaire.

Définir les paramètres
----------------------

Un Cercle peut être public ou privé selon le degré de visibilité que
vous lui accordez. Les réglages se font dans la vue principale.

![](parametres-cercle.png)

-   *Tout le monde peut demander l'adhésion* : le cercle est **ouvert**
    à tous⋅tes les utilisateur⋅ices.

-   *Les membres doivent accepter les invitations* : ajouter un⋅e membre
    déclenche l'envoi d'une **invitation qui doit être acceptée** par
    le⋅a destinataire.

-   *Les adhésions doivent être confirmées/acceptées par un modérateur
    (nécessite Open)* : les utilisateur⋅ices doivent **faire une
    demande** pour devenir
    membre.
    ![Bouton "Demander à rejoindre" dans la vue principale d'un cercle public](cercle-public.png)**La
    demande doit être acceptée** par un⋅e modérateur⋅ice du cercle. Elle
    apparaît dans les notifications.

    ![Le bouton notification est le deuxième dans le groupe à droite de la barre d'accès](notification-demande-acces-cercle.png)

    > ⚠️ Cette condition nécessite d'avoir aussi coché "Tout le monde
    > peut demander l'adhésion".

-   *Les membres peuvent aussi inviter* : **les membres ont la
    possibilité d'inviter** d'autres utilisateur⋅ices dans le Cercle
    (sans passer par un⋅e modérateur⋅ice).

-   *Visible à tous* : le cercle **apparaît dans la liste de contacts**
    de tous⋅tes les utilisateur⋅ices, ainsi que dans la **fonction
    recherche**. Si la case est décochée, seul⋅es les membres du cercle
    le verront apparaître dans leur liste.

    ![la fonction recherche est le premier bouton dans le groupe à droite de la barre d'accès](recherche-cercle.png)

------------------------------------------------------------------------

➡️ Continuer en [partageant un carnet
d'adresses](<../6. Contacts/6.3. Partager un carnet d'adresses.md>).

